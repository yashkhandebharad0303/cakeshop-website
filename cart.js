const items = [
    {
        'id': 1,
        'name': 'Sponge cake',
        'price': 599.99,
    },
    {
        'id': 2,
        'name': 'Chocolate coconut cake',
        'price': 899.99,
    },
    {
        'id': 3,
        'name': 'Carrot and walnut cake',
        'price': 799.99,
    },
    {
        'id': 4,
        'name': 'Gluten-free Persian cake',
        'price': 1199.99,
    },
    {
        'id': 5,
        'name': 'Chocolate mud cake',
        'price': 1599.99,
    },
    {
        'id': 6,
        'name': 'Hummingbird cake',
        'price': 2599.99,
    },
    {
        'id': 7,
        'name': 'Flourless chocolate cake',
        'price': 1399.99,
    },
    {
        'id': 8,
        'name': 'Layered rainbow',
        'price': 999.99,
    },
    {
        'id': 9,
        'name': 'Triple chocolate cheesecake',
        'price': 1799.99,
    },
    {
        'id': 10,
        'name': 'Choc-honeycomb ice-cream cake',
        'price': 1999.99,
    },
    {
        'id': 11,
        'name': 'Cinnamon tea cake',
        'price': 2299.99,
    },
    {
        'id': 12,
        'name': 'Double chocolate mousse cake',
        'price': 899.99,
    },
    {
        'id': 13,
        'name': 'Gluten-free chocolate loaf',
        'price': 1499.99,
    },
    {
        'id': 14,
        'name': 'White chocolate mud cake',
        'price': 1799.99,
    },
    {
        'id': 15,
        'name': 'Blueberry friands',
        'price': 1299.99,
    }
]

var TotalAmount = 0;
var ItemCount = 0;
function Addtocart(id) {
    var itemid = id;
    var item = items.find(item => item.id === itemid);
    console.log(item);
    var itemprice = item.price;
    var itemname = item.name;
    var quantity = 1;
    ItemCount += 1;
    TotalAmount += quantity * itemprice;
    console.log(TotalAmount)

    var cart = document.getElementById('carttable');
    cart.innerHTML +=
    `
    <tr>
        <th>${ItemCount}</th>
        <th>${itemname}</th>
        <th>${quantity}</th>
        <th>${itemprice}</th>
        <th>${quantity*itemprice}</th>
     </tr>
    ` 
    var carttotal = document.getElementById('carttotal');
    carttotal.innerHTML =
    `
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th><b>TOTAL</b></th>
        <th>${TotalAmount}</th>
     </tr>
    `   
}

function order(){
   var ordernow = alert('Your order has been successfully placed')
   console.log(ordernow)
}


